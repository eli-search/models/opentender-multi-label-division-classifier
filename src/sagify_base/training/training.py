from __future__ import print_function
from pathlib import Path
import joblib
import os
import json
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score, roc_auc_score, accuracy_score, coverage_error, label_ranking_average_precision_score
from sklearn.pipeline import Pipeline
from sagify.api.hyperparameter_tuning import log_metric
from sklearn.model_selection import train_test_split


def multi_label_metrics(y_true, y_pred):
    f1_micro_average = f1_score(y_true=y_true, y_pred=y_pred, average='micro')
    print(f'f1={f1_micro_average};')
    log_metric("f1", f1_micro_average)

    roc_auc = roc_auc_score(y_true, y_pred, average='micro')
    print(f'roc_auc={roc_auc};')
    log_metric("roc_auc", roc_auc)

    accuracy = accuracy_score(y_true, y_pred)
    print(f'accuracy={accuracy};')
    log_metric("accuracy", accuracy)

    coverage_err = coverage_error(y_true, y_pred)
    print(f'coverage_err={coverage_err};')
    log_metric("coverage_err", coverage_err)

    label_ranking_average_precision = label_ranking_average_precision_score(y_true, y_pred)
    print(f'label_ranking_average_precision={label_ranking_average_precision};')
    log_metric("label_ranking_average_precision", label_ranking_average_precision)


def train(input_data_path: str, model_save_path: str, hyperparams_path: str = None):
    """
    The function to execute the training.

    :param input_data_path: [str], input directory path where all the training file(s) reside in
    :param model_save_path: [str], directory path to save your model(s)
    :param hyperparams_path: [optional[str], default=None], input path to hyperparams json file.
    Example:
        {
            "max_leaf_nodes": 10,
            "n_estimators": 200
        }
    """
    print(f"Started training with input data path ({input_data_path}), model output path ({model_save_path}), "
          f"hyperparameters file path ({hyperparams_path})")

    hyperparameters = json.loads(Path(hyperparams_path).read_bytes())
    if not hyperparameters:
        raise Exception(f"Failed to load hyper parameters from path: {hyperparams_path}")
    print(f"Loaded hyper parameters from {hyperparams_path}: {hyperparameters}")

    # Load data
    input_data_folder = Path(input_data_path)
    input_files = get_filenames(input_data_folder)
    if len(input_files) == 0:
        raise ValueError(f"No file found in {input_data_folder}")
    raw_data = [pd.read_csv(file, index_col=0, engine="python") for file in input_files]
    df = pd.concat(raw_data)
    print(f"Full dataset contains {len(df.index)} records.")
    train_df, test_df = train_test_split(df, test_size=0.1)

    x_train = train_df['title_description']
    y_train = train_df.drop(['title_description'], axis=1)
    print(f"Training set contains {len(train_df.index)} records.")

    x_test = test_df['title_description']
    y_test = test_df.drop(['title_description'], axis=1)
    print(f"Test set contains {len(x_test.index)} records.")

    # Train model
    if "c_param" in hyperparameters.keys():
        c_param = float(hyperparameters.get("c_param"))
    else:
        raise Exception(f"c_param missing from hyper parameters config")

    svc_pipeline = Pipeline([
        ('tfidf', TfidfVectorizer(ngram_range=(1, 3))),
        ('clf', OneVsRestClassifier(LinearSVC(max_iter=10000, C=c_param, random_state=736283))),
    ])
    svc_pipeline.fit(x_train, y_train)

    # Evaluate model
    y_pred = svc_pipeline.predict(x_test)
    multi_label_metrics(y_test, y_pred)

    # Save model
    joblib.dump(svc_pipeline, os.path.join(model_save_path, "model.joblib"))


def get_filenames(data_folder):
    return [os.path.join(data_folder, file) for file in os.listdir(data_folder)]
