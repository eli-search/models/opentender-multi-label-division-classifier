#!/usr/bin/env bash
python3 -m pip install awscli

git_tag=$1
if [ -z "$git_tag" ]; then
  echo "Error when trying to retrieve the git tag to save the experiment. Cannot continue."
  exit 1
fi

image_name=$(bash ci/get_config_value.sh image_name)
if [ -z "$image_name" ]; then
  echo "The config 'image_name' in .sagify.json must be set. Cannot continue."
  exit 1
fi

experiment_id=$(bash ci/get_config_value.sh experiment_id)
if [ -z "$experiment_id" ]; then
  echo "The config 'experiment_id' in .sagify.json must be set. Cannot continue."
  exit 1
fi

s3_training_data_prefix="s3://d-ew1-nlex-ted-ai-ml-data/experiments/$experiment_id"
s3_data_artifacts_prefix="s3://d-ew1-nlex-ted-ai-ml-data/data-artifacts/$image_name/$git_tag"

echo "Copying data from '$s3_training_data_prefix/*' to '$s3_data_artifacts_prefix/'"
aws s3 sync "$s3_training_data_prefix" "$s3_data_artifacts_prefix"

